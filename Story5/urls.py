from django.conf.urls import url
from django.urls import path
from .views import profile, home, schedule, addschedule, clearschedule

urlpatterns = [
    path('profile', profile, name='Profile'),
    path('', home, name='Home'),
    path('home', home, name='Home'),
    path('schedule', schedule, name='Schedule'),
    path('addschedule', addschedule, name='addschedule'), # for form submission
    path('clearschedule', clearschedule, name='clearschedule') # for schedule clear
]